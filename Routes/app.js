const oracledb = require('oracledb');
const express = require('Express')
const xyz = require('../db')
var app = express()
var connection = "";
(async function(){
    connection = await oracledb.getConnection(xyz);
})()
app.get('/',(req,res)=>{
  connection.execute(
              'select * from dept_master',
              [],
            (err,result)=>{
              if(err){
                console.error(err.message);
                return;
              }
              console.log(result.rows);
              res.send(result)
            });
})
app.listen(25000,()=>{
    console.log("Listening on Port 25000")
})
module .exports=app;