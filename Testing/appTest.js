const chai = require('chai')
const app = require('../Routes/app')
const chaiHttp = require('chai-http')
// const expect = require('chai').expect;
const {expect}=chai
chai.use(chaiHttp)
describe('Routes  Test',(req,res)=>{
    it('Test Case for Helloworld',(done)=>{
        chai
            .request(app)
            .get('/')
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.text).to.equals("Hello World");
                done()
              });
            
        })
    })
